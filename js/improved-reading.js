(function ($, Drupal, once) {
  "use strict";
  let handler = null;

  Drupal.behaviors.improvedReading = {
    attach: function (context, settings) {
      handler = this;
      handler.minWordLength = settings.improvedReading.minWordLength;
      handler.minTextLength = settings.improvedReading.minTextLength;
      handler.boldRatio = settings.improvedReading.boldRatio;
      handler.jquerySelectors = $(once('applyimproved', settings.improvedReading.jquerySelectors));
      const improvedStatus = sessionStorage.getItem('improved_status');
      handler.initializeToggle(improvedStatus);

      // Apply improved reading on load.
      $(handler.jquerySelectors).each(function () {
        if (improvedStatus === 'true') {
          handler.processNode(this);
        }
      });

    },

    initializeToggle: function (improvedStatus) {
      const $toggle = $(once('improvedStatus', '#improved-toggle'));

      $toggle.each(function () {
        const $button = $(this);
        if (improvedStatus === 'true') {
          $button.attr('aria-checked', 'true');
        }

        $button.on('click', function () {
          const state = $(this).attr('aria-checked');
          if (state === 'true') {
            $button.attr('aria-checked', 'false');
            sessionStorage.setItem('improved_status', 'false')
            window.location.reload(false);
          }
          else {
            $button.attr('aria-checked', 'true');
            $(handler.jquerySelectors).each(function () {
              handler.processNode(this);
            });
            sessionStorage.setItem('improved_status', 'true')
          }
        });
      });
    },

    insertTextBefore: function (text, node, bold) {
      if (bold) {
        const strong = document.createElement("strong");
        strong.appendChild(document.createTextNode(text));
        node.parentNode.insertBefore(strong, node);
      }
      else {
        node.parentNode.insertBefore(document.createTextNode(text), node);
      }
    },

    processNode: function(node) {
      const walker = document.createTreeWalker(node, NodeFilter.SHOW_TEXT, {
        acceptNode: function (node) {
          return (
            node.parentNode.nodeName !== 'SCRIPT' &&
            node.parentNode.nodeName !== 'NOSCRIPT' &&
            node.parentNode.nodeName !== 'STYLE' &&
            node.nodeValue.length >= handler.minTextLength) ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT;
        }
      });

      while (node = walker.nextNode()) {
        let text = node.nodeValue;
        let wStart = -1, wLen = 0, eng = null;

        for (let i = 0; i <= text.length; i++) { // We use <= here because we want to include the last character in the loop
          let cEng = i < text.length ? /[\p{Letter}\p{Mark}]/u.test(text[i]) : false;

          if (i == text.length || eng !== cEng) {
            // State flipped or end of string
            if (eng && wLen >= handler.minWordLength) {
              let word = text.substring(wStart, wStart + wLen);
              let numBold = Math.ceil(word.length * handler.boldRatio);
              let bt = word.substring(0, numBold), nt = word.substring(numBold);
              handler.insertTextBefore(bt, node, true);
              handler.insertTextBefore(nt, node, false);
            } else if (wLen > 0) {
              let word = text.substring(wStart, wStart + wLen);
              handler.insertTextBefore(word, node, false);
            }
            wStart = i;
            wLen = 1;
            eng = cEng;
          } else {
            wLen++;
          }
        }
        node.nodeValue = ""; // Can't remove the node (otherwise the tree walker will break) so just set it to empty
      }
    }

  }

})(jQuery, Drupal, once);
