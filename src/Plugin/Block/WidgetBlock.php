<?php

namespace Drupal\improved_reading\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an improved reading widget block.
 *
 * @Block(
 *   id = "improved_reading_widget",
 *   admin_label = @Translation("improved Reading Widget"),
 *   category = @Translation("improved Reading")
 * )
 */
class WidgetBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('use improved_reading'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['toggle-container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['improved-toggle-container'],
      ],
    ];
    $build['toggle-container']['toggle'] = [
      '#type' => 'inline_template',
      '#template' => '<label for="improved-toggle">' . $this->t('improved reading') . '</label><button type="button" id="improved-toggle" role="switch" aria-checked="false"><span>' . $this->t('on') . '</span><span>' . $this->t('off') . '</span>
</button>',
    ];
    $build['#attached']['library'][] = 'improved_reading/widget_style';
    return $build;
  }

}
