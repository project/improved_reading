<?php

namespace Drupal\improved_reading\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure improved reading settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'improved_reading_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['improved_reading.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Enable functionality for users with the permission 'Use improved Reading'"),
      '#default_value' => $this->config('improved_reading.settings')->get('enable'),
    ];
    $form['min_word_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum word length'),
      '#description' => $this->t('Minimum text length to apply improved reading'),
      '#default_value' => $this->config('improved_reading.settings')->get('min_word_length'),
    ];
    $form['min_text_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum text length'),
      '#description' => $this->t('Minimum text length to apply improved reading'),
      '#default_value' => $this->config('improved_reading.settings')->get('min_text_length'),
    ];
    $form['bold_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bold ratio'),
      '#description' => $this->t('Ratio of the word that is bolder'),
      '#default_value' => $this->config('improved_reading.settings')->get('bold_ratio'),
    ];
    $form['jquery_selectors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('jQuery selectors'),
      '#description' => $this->t('Separate selectors with commas, e.g.: <em>h3, h4, .field--type-string, .field--type-string-long, .field--type-text-long</em>'),
      '#default_value' => $this->config('improved_reading.settings')->get('jquery_selectors'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('improved_reading.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('min_word_length', $form_state->getValue('min_word_length'))
      ->set('min_text_length', $form_state->getValue('min_text_length'))
      ->set('bold_ratio', $form_state->getValue('bold_ratio'))
      ->set('jquery_selectors', $form_state->getValue('jquery_selectors'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
