Purpose
------------

Adds a widget in a bloc to allow the user to enhance text content following the design proposal of the swiss developer Renato Casutt. See Bionic Reading® website

It adds bolder style around firsts letters of the words to speed up reading time.

Install
------------

 * Enable module
 * Enable permission for anonymous user
 * Enable feature and configure it in settings form
 * Add toggle button bloc

E.g.
------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

**Lo**rem **ip**sum **do**lor **s**it **a**met, **cons**ectetur **adi**piscing **e**lit, **s**ed do **eiu**smod **te**mpor **inc**ididunt ut **la**bore et **do**lore **ma**gna **al**iqua.
